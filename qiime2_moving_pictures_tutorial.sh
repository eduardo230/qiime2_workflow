# Setup
# mkdir qiime2-moving-pictures-tutorial
# cd qiime2-moving-pictures-tutorial/
# wget https://data.qiime2.org/2019.7/tutorials/moving-pictures/sample_metadata.tsv
# mkdir emp-single-end-sequences
# cd emp-single-end-sequences/
# wget   -O "emp-single-end-sequences/barcodes.fastq.gz"   "https://data.qiime2.org/2019.7/tutorials/moving-pictures/emp-single-end-sequences/barcodes.fastq.gz"
# wget   -O "emp-single-end-sequences/sequences.fastq.gz"   "https://data.qiime2.org/2019.7/tutorials/moving-pictures/emp-single-end-sequences/sequences.fastq.gz"
# wget https://data.qiime2.org/2019.7/common/gg-13-8-99-515-806-nb-classifier.qza

# All data that is used as input to QIIME 2 is in form of QIIME 2 artifacts, 
# which contain information about the type of data and the source of the data.
qiime tools import \
  --type EMPSingleEndSequences \
  --input-path emp-single-end-sequences \
  --output-path emp-single-end-sequences.qza

#To demultiplex sequences we need to know which barcode sequence is associated with each sample. 
# This information is contained in the sample metadata file.
qiime demux emp-single \
  --i-seqs emp-single-end-sequences.qza \
  --m-barcodes-file sample_metadata.tsv \
  --m-barcodes-column barcode-sequence \
  --o-per-sample-sequences demux.qza \
  --o-error-correction-details demux-details.qza

# how many sequences were obtained per sample
qiime demux summarize \
  --i-data demux.qza \
  --o-visualization demux.qzv

# QIIME 2 plugins are available for several quality control methods
# Option 1: DADA2 a pipeline for detecting and correcting (where possible) Illumina amplicon sequence data
qiime dada2 denoise-single \
  --i-demultiplexed-seqs demux.qza \
  --p-trim-left 0 \
  --p-trunc-len 120 \
  --o-representative-sequences rep-seqs-dada2.qza \
  --o-table table-dada2.qza \
  --o-denoising-stats stats-dada2.qza

qiime metadata tabulate \
  --m-input-file stats-dada2.qza \
  --o-visualization stats-dada2.qzv

# Option 2: Deblur
qiime quality-filter q-score \
 --i-demux demux.qza \
 --o-filtered-sequences demux-filtered.qza \
 --o-filter-stats demux-filter-stats.qza

# Next, the Deblur workflow is applied using the qiime deblur denoise-16S method.
qiime deblur denoise-16S \
  --i-demultiplexed-seqs demux-filtered.qza \
  --p-trim-length 120 \
  --o-representative-sequences rep-seqs-deblur.qza \
  --o-table table-deblur.qza \
  --p-sample-stats \
  --o-stats deblur-stats.qza

qiime metadata tabulate \
  --m-input-file demux-filter-stats.qza \
  --o-visualization demux-filter-stats.qzv

qiime deblur visualize-stats \
  --i-deblur-stats deblur-stats.qza \
  --o-visualization deblur-stats.qzv

# continue analysis using this FeatureTable
mv rep-seqs-deblur.qza rep-seqs.qza
mv table-deblur.qza table.qza

# summarize command will give you information on how many sequences are associated 
# with each sample and with each feature, histograms of those distributions, 
# and some related summary statistics. 
qiime feature-table summarize \
  --i-table table.qza \
  --o-visualization table.qzv \
  --m-sample-metadata-file sample_metadata.tsv

# tabulate-seqs command will provide a mapping of feature IDs to sequences, 
# and provide links to easily BLAST each sequence against the NCBI nt database. 
qiime feature-table tabulate-seqs \
  --i-data rep-seqs.qza \
  --o-visualization rep-seqs.qzv

# the pipeline uses the mafft program to perform a multiple sequence alignment of the sequences
# filters the alignment to remove positions that are highly variable. 
# applies FastTree to generate a phylogenetic tree from the masked alignment.
qiime phylogeny align-to-tree-mafft-fasttree \
  --i-sequences rep-seqs.qza \
  --o-alignment aligned-rep-seqs.qza \
  --o-masked-alignment masked-aligned-rep-seqs.qza \
  --o-tree unrooted-tree.qza \
  --o-rooted-tree rooted-tree.qza 

# Alpha diversity
# Beta diversity
qiime diversity core-metrics-phylogenetic \
  --i-phylogeny rooted-tree.qza \
  --i-table table.qza \
  --p-sampling-depth 1103 \
  --m-metadata-file sample_metadata.tsv \
  --output-dir core-metrics-results

# test for associations between categorical metadata columns and alpha diversity data. 
# We’ll do that here for the Faith Phylogenetic Diversity (a measure of community richness) and evenness metrics.
qiime diversity alpha-group-significance \
  --i-alpha-diversity core-metrics-results/faith_pd_vector.qza \
  --m-metadata-file sample_metadata.tsv \
  --o-visualization core-metrics-results/faith-pd-group-significance.qzv

qiime diversity alpha-group-significance \
  --i-alpha-diversity core-metrics-results/evenness_vector.qza \
  --m-metadata-file sample_metadata.tsv \
  --o-visualization core-metrics-results/evenness-group-significance.qzv

# analyze sample composition in the context of categorical metadata using PERMANOVA
# using the beta-group-significance command
qiime diversity beta-group-significance \
  --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file sample_metadata.tsv \
  --m-metadata-column body-site \
  --o-visualization core-metrics-results/unweighted-unifrac-body-site-significance.qzv \
  --p-pairwise

qiime diversity beta-group-significance \
  --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file sample_metadata.tsv \
  --m-metadata-column subject \
  --o-visualization core-metrics-results/unweighted-unifrac-subject-group-significance.qzv \
  --p-pairwise

# Emperor tool to explore principal coordinates (PCoA) plots in the context of sample metadata. 
qiime emperor plot \
  --i-pcoa core-metrics-results/unweighted_unifrac_pcoa_results.qza \
  --m-metadata-file sample_metadata.tsv \
  --p-custom-axes days-since-experiment-start \
  --o-visualization core-metrics-results/unweighted-unifrac-emperor-days-since-experiment-start.qzv

qiime emperor plot \
  --i-pcoa core-metrics-results/bray_curtis_pcoa_results.qza \
  --m-metadata-file sample_metadata.tsv \
  --p-custom-axes days-since-experiment-start \
  --o-visualization core-metrics-results/bray-curtis-emperor-days-since-experiment-start.qzv

# explore alpha diversity as a function of sampling depth using the qiime diversity alpha-rarefaction visualizer.
qiime diversity alpha-rarefaction \
  --i-table table.qza \
  --i-phylogeny rooted-tree.qza \
  --p-max-depth 4000 \
  --m-metadata-file sample_metadata.tsv \
  --o-visualization alpha-rarefaction.qzv

# explore the taxonomic composition of the samples, and again relate that to sample metadata.
qiime feature-classifier classify-sklearn \
  --i-classifier gg-13-8-99-515-806-nb-classifier.qza \
  --i-reads rep-seqs.qza \
  --o-classification taxonomy.qza

qiime metadata tabulate \
  --m-input-file taxonomy.qza \
  --o-visualization taxonomy.qzv

# view the taxonomic composition of our samples with interactive bar plots
qiime taxa barplot \
  --i-table table.qza \
  --i-taxonomy taxonomy.qza \
  --m-metadata-file sample_metadata.tsv \
  --o-visualization taxa-bar-plots.qzv

# ANCOM can be applied to identify features that are differentially abundant across sample groups.
qiime feature-table filter-samples \
  --i-table table.qza \
  --m-metadata-file sample_metadata.tsv \
  --p-where "[body-site]='gut'" \
  --o-filtered-table gut-table.qza

qiime composition add-pseudocount \
  --i-table gut-table.qza \
  --o-composition-table comp-gut-table.qza

# run ANCOM on the subject column to determine what features differ in abundance across the gut samples of the two subjects.
qiime composition ancom \
  --i-table comp-gut-table.qza \
  --m-metadata-file sample_metadata.tsv \
  --m-metadata-column subject \
  --o-visualization ancom-subject.qzv

# interested in performing a differential abundance test at a specific taxonomic level. 
# To do this, we can collapse the features in our FeatureTable[Frequency] at the taxonomic level of interest, 
# and then re-run the above steps.
qiime taxa collapse \
  --i-table gut-table.qza \
  --i-taxonomy taxonomy.qza \
  --p-level 6 \
  --o-collapsed-table gut-table-l6.qza

qiime composition add-pseudocount \
  --i-table gut-table-l6.qza \
  --o-composition-table comp-gut-table-l6.qza

qiime composition ancom \
  --i-table comp-gut-table-l6.qza \
  --m-metadata-file sample_metadata.tsv \
  --m-metadata-column subject \
  --o-visualization l6-ancom-subject.qzv

#multiqc .
